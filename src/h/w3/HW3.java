package h.w3;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import jdk.nashorn.internal.objects.Global;

public class HW3 {

    private static HashMap <String,HashMap <String,Integer>> allWords = new HashMap<String, HashMap<String, Integer>>();
    private static String FileName = null;
    private static File file = new File("Reuters21578-Apte-90Cat/training");
    private static String [] tempWord;
    private static String tempLine;
    private static BufferedReader BR = null;
    private static BufferedWriter BW = null;
    private static HashMap <String,Integer> stopWords = new HashMap <String,Integer> ();
    private static HashMap <String,Double> prior = new HashMap <String,Double> ();
    private static HashMap <String,Long> wordConterForEveryClass = new HashMap <String,Long> ();
    private static Long counterPrior = 0L; 
    private static HashMap <String,HashMap <String,Float>> allWordsinFilesProb = new HashMap<String,HashMap<String, Float>>();
    private static int numberOfTypes = 0;
    private static HashMap <String,Integer> allWordsInOneHash = new HashMap <String,Integer>();
    private static String wordAfterStem = null;
    private static HashMap <String,HashMap<String,Integer>> allWordsForTest = new HashMap <String,HashMap<String,Integer>> ();
    private static HashMap <String,HashMap<String,Double>> allTestFilesProb = new HashMap <String,HashMap<String,Double>>();
    private static HashMap <String,String> testPredection = new HashMap<String,String>();
    private static Double prob = Double.MIN_VALUE;
    private static String classNameForTest;
    private static Double prevProb;
    private static Double addToProb;
    
    public static void main(String[] args) throws IOException {
  
        readStopWords();
        
        System.out.print("Now Reading Text Files \n");
        filesInDir(file);
        System.out.print("Finish ! \n");
        
        System.out.println("Calcualte Prior For Every Class");
        for (String temp : prior.keySet())
            prior.put(temp, prior.get(temp) / counterPrior);
        
        System.out.println("Finish !");                                         
        
        numberOfTypes = allWordsInOneHash.size();
        
        System.out.println(numberOfTypes);
        
        System.out.println("Calcualte Prob.");
        for (String tempFile : allWords.keySet()){
            
            allWordsinFilesProb.put(tempFile, new HashMap <String,Float> ());
            for (String tempWord : allWords.get(tempFile).keySet())
                allWordsinFilesProb.get(tempFile)
                        .put(tempWord, (allWords.get(tempFile).get(tempWord) + 1F) / 
                                (numberOfTypes + wordConterForEveryClass.get(tempFile)));
                                                                   
                                                 }//for loop
        
        for (String tempFile : allWordsinFilesProb.keySet()){
            
            file = new File (tempFile);
            BW = new BufferedWriter(new FileWriter(file.getName()+".txt"));
            
            BW.write("" + prior.get(tempFile));
            BW.write(System.lineSeparator());
            
            for (String tempWord : allWordsinFilesProb.get(tempFile).keySet()){
               
                BW.write(tempWord + " : " + allWordsinFilesProb.get(tempFile).get(tempWord));
                BW.write(System.lineSeparator());
                                                                              }//for loop
            
            BW.close();
            
                                                            }//for loop
        
        
        file = new File("Reuters21578-Apte-90Cat/test");
        
        filesInDirToTest(file);
        
        for (String tempFileName : allWordsForTest.keySet()){
                        
            allTestFilesProb.put(tempFileName, new HashMap<String,Double>());
            
            for (String tempFileSubName : allWords.keySet())
                allTestFilesProb.get(tempFileName).put(tempFileSubName, /*Math.log*/(prior.get(tempFileSubName)));
              
            
            for (String tempWord : allWordsForTest.get(tempFileName).keySet()){
                
                for (String className : allWordsinFilesProb.keySet()){
                       
                    if (allWordsinFilesProb.get(className).containsKey(tempWord)){
                        
                        
                        prevProb = allTestFilesProb.get(tempFileName).get(className);
                        addToProb = Math.log10(allWordsinFilesProb.get(className).get(tempWord) + 1D)
                                / (wordConterForEveryClass.get(className) + numberOfTypes);
                        
                        allTestFilesProb.get(tempFileName).put(className, prevProb + addToProb);
                                                                                 }
                    
                    else {
                        
                        prevProb = allTestFilesProb.get(tempFileName).get(className);
                        addToProb = Math.log10(1D/(wordConterForEveryClass.get(className) + numberOfTypes + 1));
                        
                        allTestFilesProb.get(tempFileName).put(className, prevProb + addToProb);
                        
                         }
                                                                     }//for loop
                    
                
                                                                              }//for loop
            prob = -Global.Infinity;
            for (String tempProb : allTestFilesProb.get(tempFileName).keySet()){
                if (allTestFilesProb.get(tempFileName).get(tempProb) > prob){
                    prob = allTestFilesProb.get(tempFileName).get(tempProb);
                    classNameForTest = tempProb;
                                                                            }//if statement
                    
                                                                               }//for loop
            testPredection.put(tempFileName, classNameForTest);
            
                                                    }//for loop
        String tempClass;
        int i = 0;
        System.out.println("Test Predection : ");
        for (String temp : testPredection.keySet()){
            tempClass = temp.substring(0,temp.indexOf("/"));
            if (tempClass.equals(testPredection.get(temp)))
                i++;
                                                   }//for loop
        
        System.out.println((i + 0.0) / testPredection.size());
        
                                                              }//main
                      
    
    public static void readStopWords () throws FileNotFoundException, IOException{
        
        BR = new BufferedReader(new InputStreamReader(new FileInputStream("Stop Words.txt")));
        
        while ((tempLine = BR.readLine()) != null)
            stopWords.put(tempLine, 0);
        
        BR.close();
                                                                                  }//readStopWords
   
    public static void filesInDir (File directory) throws IOException{
        
        File [] Temp = directory.listFiles();
        for (File Temp1 : Temp) 
            if (Temp1.isFile()){
                
                counterPrior++;
                
                FileName = Temp1.getParentFile().getName();

                if (!allWords.containsKey(FileName))
                    allWords.put(FileName, new HashMap<String,Integer>());
             
                if (!prior.containsKey(FileName))
                    prior.put(FileName, 1D);
                
                else
                    prior.put(FileName, prior.get(FileName) + 1);
                
                
                BR = new BufferedReader(new InputStreamReader(new FileInputStream(Temp1)));
                
                while ((tempLine = BR.readLine())!= null){
                    tempWord = tempLine.split("[\\s -]");
                    
                    for (int i = 0 ; i < tempWord.length ; i++){
                        if (!tempWord[i].isEmpty() && !stopWords.containsKey(tempWord[i]))
                            wordAfterStem = stemWords(tempWord[i]);                        
                        
                        if (wordAfterStem != null && !wordAfterStem.isEmpty() && !wordAfterStem.equals(" ")){
                            
                            if (!allWords.get(FileName).containsKey(wordAfterStem))
                                allWords.get(FileName).put(wordAfterStem, 1);
                        
                            else
                                allWords.get(FileName).put(wordAfterStem, allWords
                                        .get(FileName).get(wordAfterStem) + 1);
                        
                            if (!allWordsInOneHash.containsKey(wordAfterStem))
                                allWordsInOneHash.put(wordAfterStem, 1);
                        
                            else
                                allWordsInOneHash.put(wordAfterStem, allWordsInOneHash.get(wordAfterStem) + 1);
                        
                            if (!wordConterForEveryClass.containsKey(FileName))
                                wordConterForEveryClass.put(FileName, 1L);
                
                            else
                                wordConterForEveryClass.put(FileName, wordConterForEveryClass.get(FileName) + 1);
                            
                                                                                     }//if statement
                                                               }//for loop
                    
                                                         }//while loop
                
                               }//if statement 
                                
             else 
                filesInDir(Temp1);
                                                                      }//filesInDir 
    
    public static void filesInDirToTest (File directory) throws IOException{
        
        File [] Temp = directory.listFiles();
        
        
        for (File Temp1 : Temp)    
            if (Temp1.isFile()){
                
                FileName = Temp1.getParentFile().getName() + "/" + Temp1.getName();
                
                allWordsForTest.put(FileName, new HashMap<String,Integer> ());
                
                BR = new BufferedReader(new InputStreamReader(new FileInputStream(Temp1)));
                
                while ((tempLine = BR.readLine())!= null){
                    tempWord = tempLine.split("[\\s -]");
                    
                    for (int i = 0 ; i < tempWord.length ; i++){
                        if (!tempWord[i].isEmpty() && !stopWords.containsKey(tempWord[i]))
                            wordAfterStem = stemWords(tempWord[i]);                        
                        
                        if (wordAfterStem != null && !wordAfterStem.isEmpty() && !wordAfterStem.equals(" ")){
                                
                            if (!allWordsForTest.get(FileName).containsKey(wordAfterStem))
                                allWordsForTest.get(FileName).put(wordAfterStem, 1);
                            
                            else 
                                allWordsForTest.get(FileName).put(wordAfterStem, 
                                        allWordsForTest.get(FileName).get(wordAfterStem) + 1);
                            
                                                                                                            }//if statement
                        
                                                                }//for loop
                    
                                                          }//while loop
               
                               }//if statement
            else
                filesInDirToTest(Temp1);
                
        
                                                                            }//filesInDirToTest
    
    public static String stemWords (String statemet){
        
        Integer count;
        
        int counter , sT;
        
        String temp = "";
        statemet += " ";
        PorterStemmer stemmer = new PorterStemmer();
        
        for (int i = 0 ; i < statemet.length() ;){
            
            sT = statemet.charAt(i);
            
            if (!Character.isLetter(sT)){
                i++;
                continue;
                                        }//if statement
            
            if (Character.isLetter((char)sT)){
                
                counter = 0;
                
                while (i < statemet.length()){
                    
                    sT = Character.toLowerCase((char)sT);
                    temp += (char)sT;
                    if (counter < 500)
                        counter++;
                    i++;
                    sT = statemet.charAt(i);

                    if (!Character.isLetter((char) sT)) {
                                                         
                        for (int c = 0; c < counter; c++)
                            stemmer.add(temp.charAt(c));
                        
                        stemmer.stem();
                        
                        if (!temp.equals("")){
                            
                            temp = stemmer.toString();
                            
                                             }//if statement
                        
                        
                        i++;
                        break;
                                                        }//if statement
                    
                             }//while loop
                
                                               }//if statement
            
                    if (sT < 0)
                        break;
                 
                              
                    
                       }//while loop
     
        
        
        return temp;
        
                                                        }//stemToPredect
          
                 }//HW3
